<?php

/**
 * @file
 * Provides functionality to see content sent to Tripolis.
 */

/**
 * Table with articles sent to Tripolis.
 *
 * @return string
 *   HTML for table.
 */
function tripolis_articles() {
  $workspaces = _tripolis_get_workspaces();

  $headers = array(
    'article' => array('data' => t('Article')),
    'workspace' => array('data' => t('Sent to workspace')),
    'action' => array('date' => t('Edit')),
  );

  $articles_array = array();
  $articles_query_results = tripolis_get_all_article_workspace_combinations();
  foreach ($articles_query_results as $articles_query_result) {
    $workspace_name = $workspaces[$articles_query_result->workspace_id]['label'];
    $nid = $articles_query_result->nid;
    $node = node_load($nid);
    $title = $node->title;
    $path = $node->path['source'];

    if (!isset($articles_array[$articles_query_result->nid])) {
      $articles_array[$articles_query_result->nid] = array(
        'article' => l($title, $path),
        'workspace' => $workspace_name,
        'action' => l(t('edit'), $path . '/edit'),
      );
    }
    else {
      $articles_array[$articles_query_result->nid]['workspace'] = $articles_array[$articles_query_result->nid]['workspace'] . ', ' . $workspace_name;
    }
  }

  return theme('table', array(
    'header' => $headers,
    'rows' => $articles_array,
    'attributes' => array('class' => array('invited_users')),
    '#empty' => t('No invited users yet'),
  ));
}

/**
 * Get all rows form tripolis_article_workspace table.
 *
 * @return array
 *   Result from database containing key ID, Node ID and Workspace ID.
 */
function tripolis_get_all_article_workspace_combinations() {
  $results_array = array();
  $query = db_select('tripolis_article_workspace', 'taw')
    ->fields('taw', array('taid'))
    ->fields('taw', array('nid'))
    ->fields('taw', array('workspace_id'));

  $result = $query->execute();

  if ($result) {
    foreach ($result as $record) {
      $results_array[] = $record;
    }
  }

  return $results_array;
}
