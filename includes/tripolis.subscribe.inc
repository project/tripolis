<?php
/**
 * @file
 * Provides subscription form based on contactgroups from Tripolis.
 *
 * This concerns mailing categories within Tripolis.
 */

/**
 * Render page (form) for mailing subscription.
 *
 * @return array
 *   Subscription form.
 */
function tripolis_subscribe($form, &$form_state, $set_page_title = FALSE) {
  $form = array();
  $vars = variable_get('tripolis_subscribe_form_variables');
  $vars = unserialize($vars);

  if ($set_page_title && !empty($vars['page_title'])) {
    drupal_set_title($vars['page_title']);
  }

  $form['intro_text'] = array(
    '#markup' => $vars['introduction']['value'],
  );

  // Email address.
  $form['subscriber_email_address'] = array(
    '#type'             => 'textfield',
    '#title'            => t('E-mail address'),
    '#required'         => TRUE,
    '#attributes'       => array('placeholder' => t('E-mail address')),
    '#element_validate' => array('_tripolis_validate_email'),
  );

  // Fetch groups from Tripolis.
  $contact_groups = _tripolis_get_groups();

  $form['category_title'] = array(
    '#markup' => theme('html_tag', array(
      'element' => array(
        '#tag'   => 'h2',
        '#value' => t('Newsletters'),
      ),
    )),
  );

  // Compose nested form elements from list.
  $hierarchical_groups = array();
  _tripolis_hierarchical_groups_to_form($hierarchical_groups, $contact_groups, TRUE);
  if (!empty($hierarchical_groups)) {
    $form['categories'] = $hierarchical_groups;
  }
  else {
    // Notify visitor that there are no subscriptions to make.
    return array(
      'not' => array(
        '#type'   => 'markup',
        '#markup' => t('At this moment there are no newsletters to subscribe for.'),
      ),
    );
  }

  // Add submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * Validates email addresses.
 *
 * @param array $element
 *   The element to be checked.
 * @param array $form_state
 *   An array with the form states.
 * @param array $form
 *   An array containing the form.
 */
function _tripolis_validate_email(array $element, array &$form_state, array $form) {
  if (!valid_email_address($element['#value'])) {
    form_error($element, t('Please enter a valid email address.'));
  }
}

/**
 * Validate subscription form.
 *
 * Save values as variable.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function tripolis_subscribe_validate(array $form, array &$form_state) {
  $groups_display = variable_get('tripolis_contact_groups_display', array());

  // Validation on fields.
  $empty = TRUE;
  foreach ($form_state['input'] as $id => $field) {
    if ($tripolis_id = _tripolis_extract_tripolis_id($id)) {
      // General emptiness check. (At least 1 item should be selected).
      if (isset($groups_display[$tripolis_id]) && $field == 1) {
        $empty = FALSE;
      }
    }
  }

  // Set error if no mailing cat was selected at all.
  if ($empty) {
    form_error($form['categories'], t('Select at least one category.'));
  }
}

/**
 * Submit handler.
 *
 * Save values as variable.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function tripolis_subscribe_submit(array $form, array &$form_state) {
  $result = FALSE;
  $email = $form_state['input']['subscriber_email_address'];

  // Add chosen groups to list.
  $groups_display = variable_get('tripolis_contact_groups_display', array());
  $contact_groups = array();
  foreach ($form_state['input'] as $id => $field) {
    // Extract tripolis ID and check if contact-group is enabled.
    if ($tripolis_id = _tripolis_extract_tripolis_id($id)) {
      if (isset($groups_display[$tripolis_id]) && $field == 1) {
        $contact_groups[] = $tripolis_id;
      }
    }
  }

  // See if contact already exists in Tripolis DB, or create new one.
  $auth = tripolis_get_auth();
  if (($library = libraries_load('tripolis')) && !empty($library['loaded'])) {
    $tripolis = new TripolisContactService($auth['client'], $auth['username'], $auth['password']);
    $tripolis->setDbId($auth['db']);
    $contact = $tripolis->searchByDefaultContactField($email);
    if (is_array($contact) && isset($contact[0]['contactId'])) {
      // Contact already exists!
      $contact_id = $contact[0]['contactId'];
    }
    else {
      // Create contact.
      $fields = array();
      $fields[] = array(
        'name' => 'email',
        'value' => $email,
      );
      $contact_id = $tripolis->create($fields);
    }

    // Subscribe.
    if ($contact_id) {
      $result = $tripolis->addToContactGroup($contact_id, $contact_groups, $confirmed = TRUE);
    }

    if ($result) {
      $vars = variable_get('tripolis_subscribe_form_variables');
      $vars = unserialize($vars);

      $msg = t('Thank you for subscribing to our newsletter!');
      if (!empty($vars['response'])) {
        $msg = $vars['response'];
      }

      drupal_set_message($msg);
    }
    else {
      watchdog('tripolis', 'Error occurred during subscription: email: @email', array('@email' => $email), WATCHDOG_WARNING);
      drupal_set_message(t('An error occurred with your subscription. Please try again later.'));
    }
  }

  // Used for tests.
  return $result;
}
