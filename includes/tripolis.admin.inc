<?php
/**
 * @file
 * Admin form.
 */

/**
 * Provide admin form to turn on / off tripolis contact groups.
 *
 * @return array
 *   Form array.
 */
function tripolis_admin_categories() {
  $form = array();

  // Strip parent leven option.
  $form['strip_parent_level'] = array(
    '#title'            => t('Strip top levels from subscription form'),
    '#type'             => 'textfield',
    '#description'      => t('This hides the N top levels in the hierarchical select structure on the subscription form.'),
    '#default_value'    => variable_get('tripolis_strip_top_level', 1),
    '#element_validate' => array('element_validate_number'),
  );

  // Fetch groups from Tripolis.
  $contact_groups = _tripolis_get_groups($reset = TRUE);

  // Compose nested form elements from list.
  if ($contact_groups) {
    _tripolis_hierarchical_groups_to_form($form, $contact_groups);
  }
  else {
    drupal_set_message(t('Unable to fetch data from API. Check credentials and database ID etc.'));
  }

  // Add submit button.
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit handler.
 *
 * Save values as variable.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function tripolis_admin_categories_submit(array $form, array &$form_state) {
  // Save strip_top_level var.
  variable_set('tripolis_strip_top_level', $form_state['input']['strip_parent_level']);

  // Add chosen groups to list.
  // Compare form values with contact groups, to distinct
  // contact groups from other form values.
  $auth = tripolis_get_auth();

  // Note that this cache always exists because it's build up during
  // form rendering phase.
  $groups = cache_get('tripolis_groups_flat_' . $auth['db']);
  $groups = $groups->data;
  $contact_groups = array();
  foreach ($form_state['input'] as $element => $field) {
    // Check if contact-group is enabled.
    if ($id = _tripolis_extract_tripolis_id($element)) {
      if (isset($groups[$id])) {
        $contact_groups[$id] = $field;
      }
    }
  }

  // Only save group configuration if there are groups in the system.
  // If not, assume authentication was wrong, or database ID
  // does not exist in tripolis.
  // If we don't do this, old selection is overridden
  // with black, meaningless emptiness :(.
  if (!empty($contact_groups)) {
    variable_set('tripolis_contact_groups_display', $contact_groups);
  }
}
