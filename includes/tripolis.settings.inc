<?php
/**
 * @file
 * Admin form.
 */

/**
 * Helper function: Configure tripolis subscription form.
 */
function tripolis_settings_form(array $form, array &$form_state)
{
  $vars = variable_get('tripolis_subscribe_form_variables');
  $vars = unserialize($vars);
  $node_types = array();

  foreach (node_type_get_types() as $node_type) {
    $node_types[$node_type->type] = $node_type->name;
  }

  $form['heading'] = array(
    '#markup' => theme('html_tag', array(
      'element' => array(
        '#tag' => 'h2',
        '#value' => t('Tripolis settings'),
      ),
    )),
  );

  $form['subscription_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription form settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['subscription_form']['tripolis_pagetitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Page title'),
    '#default_value' => (isset($vars['page_title'])) ? $vars['page_title'] : '',
    '#required' => FALSE,
    '#description' => t('Please enter the title of the add news page.'),
  );

  $form['subscription_form']['tripolis_introduction'] = array(
    '#type' => 'text_format',
    '#title' => t('Introduction'),
    '#default_value' => (isset($vars['introduction']['value'])) ? $vars['introduction']['value'] : '',
    '#format' => $vars['introduction']['format'],
    '#required' => FALSE,
    '#description' => t('Please enter an intro text to display before the form.'),
  );

  $form['subscription_form']['tripolis_response'] = array(
    '#type' => 'textarea',
    '#title' => t('Response'),
    '#default_value' => (isset($vars['response'])) ? $vars['response'] : '',
    '#required' => TRUE,
    '#description' => t('Please enter the response message.'),
  );

  $form['content_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['content_settings']['intro'] = array(
    '#markup' => '<p>' . t('Select the content types to add Tripolis articles from.') . '</p>',
  );

  foreach (node_type_get_types() as $node) {
    $node_type = $node->type;
    $node_name = $node->name;

    $var_node_type = $vars['node_type'][$node_type];
    $fields = field_info_instances('node', $node_type);

    $fields_name_array = array(-2 => t('No mapping'), -1 => t('Title'));

    $field_types = array('text_long', 'text', 'datetime');
    foreach ($fields as $field) {
      $field_type = field_info_field($field['field_name'])['type'];
      if (in_array($field_type, $field_types)) {
        $fields_name_array[$field['id']] = $field['label'];
      }
    }

    $var_node_type['switch'] = (isset($var_node_type['switch'])) ? $var_node_type['switch'] : 0;

    $form['content_settings']['node_type_' . $node_type] = array(
      '#type' => 'fieldset',
      '#title' => $node_name,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['content_settings']['node_type_' . $node_type]['intro'] = array(
      '#markup' => '<p>' . t('Select a mapping of content type fields to the Tripolis article. The Tripolis article will be based on the "1kolomsartikel" template') . '</p>',
    );

    $form['content_settings']['node_type_' . $node_type]['tripolis_node_types_' . $node_type] = array(
      '#title' => t('Select @contenttype as content type to add Tripolis articles from.', array('@contenttype' => $node_name)),
      '#type' => 'checkbox',
      '#default_value' => $var_node_type['switch'],
    );

    $var_node_type['select_node_types_theme_' . $node_type] = (isset($var_node_type['select_node_types_theme_' . $node_type])) ? $var_node_type['select_node_types_theme_' . $node_type] : -1;
    $var_node_type['select_node_types_title_' . $node_type] = (isset($var_node_type['select_node_types_title_' . $node_type])) ? $var_node_type['select_node_types_title_' . $node_type] : -1;
    $var_node_type['select_node_types_text_' . $node_type] = (isset($var_node_type['select_node_types_text_' . $node_type])) ? $var_node_type['select_node_types_text_' . $node_type] : -1;

    $form['content_settings']['node_type_' . $node_type]['select_node_types_theme_' . $node_type] = array(
      '#title' => t('Theme in Tripolis'),
      '#type' => 'select',
      '#options' => $fields_name_array,
      '#default_value' => $var_node_type['select_node_types_theme_' . $node_type],
    );
    $form['content_settings']['node_type_' . $node_type]['select_node_types_title_' . $node_type] = array(
      '#title' => t('Title in Tripolis'),
      '#type' => 'select',
      '#options' => $fields_name_array,
      '#default_value' => $var_node_type['select_node_types_title_' . $node_type],
    );
    $form['content_settings']['node_type_' . $node_type]['select_node_types_text_' . $node_type] = array(
      '#title' => t('Text in Tripolis'),
      '#type' => 'select',
      '#options' => $fields_name_array,
      '#default_value' => $var_node_type['select_node_types_text_' . $node_type],
      '#description' => t('Available images will be added to this field.'),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Helper function: Configure tripolis subscription form submit handler.
 */
function tripolis_settings_form_submit(array $form, array &$form_state)
{
  $var = array();
  $var['page_title'] = $form_state['values']['tripolis_pagetitle'];
  $var['response'] = $form_state['values']['tripolis_response'];
  $var['introduction'] = $form_state['values']['tripolis_introduction'];

  foreach (node_type_get_types() as $node_type) {
    $var['node_type'][$node_type->type]['switch'] = $form_state['values']['tripolis_node_types_' . $node_type->type];
    $var['node_type'][$node_type->type]['select_node_types_theme_' . $node_type->type] = $form_state['values']['select_node_types_theme_' . $node_type->type];
    $var['node_type'][$node_type->type]['select_node_types_title_' . $node_type->type] = $form_state['values']['select_node_types_title_' . $node_type->type];
    $var['node_type'][$node_type->type]['select_node_types_text_' . $node_type->type] = $form_state['values']['select_node_types_text_' . $node_type->type];
  }

  $var = serialize($var);
  variable_set('tripolis_subscribe_form_variables', $var);
  drupal_set_message(t('Settings saved.'));
}

/**
 * Helper function: Configure subscribe blocks.
 */
function tripolis_admin_blocks()
{
  $form['heading'] = array(
    '#markup' => theme('html_tag', array(
      'element' => array(
        '#tag' => 'h2',
        '#value' => t('Block settings.'),
      ),
    )),
  );
  $form['intro_text'] = array(
    '#markup' => '<p>' . t('Manage content for the subscribe blocks.') . '</p>',
  );

  $form['fs_subscribe_link'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscribe link block'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $name = 'tripolis_subscribe_link_title';
  $content = variable_get($name, '');
  $form['fs_subscribe_link'][$name] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $content,
    '#description' => t('Leave empty to hide the block.'),
  );
  $name = 'tripolis_subscribe_link_text';
  $content = variable_get($name, array('value' => '', 'format' => NULL));
  $form['fs_subscribe_link'][$name] = array(
    '#type' => 'text_format',
    '#title' => t('Text'),
    '#default_value' => $content['value'],
    '#format' => 'filtered_html',
  );
  $name = 'tripolis_subscribe_link_link';
  $content = variable_get($name, '');
  $form['fs_subscribe_link'][$name] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => $content,
    '#description' => t('Leave empty if there should not be a link.'),
  );

  $form['fs_subscribe_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscribe form block'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $name = 'tripolis_subscribe_form_title';
  $content = variable_get($name, '');
  $form['fs_subscribe_form'][$name] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $content,
  );

  return system_settings_form($form);
}
